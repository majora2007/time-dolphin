using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {

    public GUISkin guiSkin;
    public float xOffset = 10.0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        GUI.skin = guiSkin;
        // Lives (far left)            Score (middle)      Fish        Pause button (far right)
        Vector2 textSize = GUI.skin.GetStyle("Label").CalcSize(new GUIContent("LIVES"));
        GUI.BeginGroup(new Rect(xOffset, 0, Screen.width, 100));

        GUI.Label(new Rect(xOffset, 0, textSize.x, textSize.y), "LIVES");
        GUI.Label(new Rect(xOffset, textSize.y, textSize.x, textSize.y), Runner.Lives + "");

        textSize = GUI.skin.GetStyle("Label").CalcSize(new GUIContent("SCORE"));
        GUI.Label(new Rect(Screen.width / 2, 0, textSize.x, textSize.y), "SCORE");
        GUI.Label(new Rect(Screen.width / 2, textSize.y, textSize.x, textSize.y), Mathf.FloorToInt(Runner.DistanceTraveled * 10) + "");

        textSize = GUI.skin.GetStyle("Label").CalcSize(new GUIContent("FISHES"));
        GUI.Label(new Rect(Screen.width / 4 * 3, 0, textSize.x, textSize.y), "FISHES");
        GUI.Label(new Rect(Screen.width / 4 * 3, textSize.y, textSize.x, textSize.y), Runner.FishCollected + "");


        textSize = GUI.skin.GetStyle("Label").CalcSize(new GUIContent("PAUSE"));
        GUI.Label(new Rect(Screen.width - (textSize.x + 25) - xOffset, 0, textSize.x, textSize.y), "PAUSE");
        if (GUI.Button(new Rect(Screen.width - (textSize.x + 25) - xOffset, textSize.y, textSize.x + 25, textSize.y + 25), "", GUI.skin.GetStyle("pausebutton")))
        {
            // Toggle Pause Game
            Debug.Log("Pause Game pressed.");
        }


        GUI.EndGroup();
    }
}
