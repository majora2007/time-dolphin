using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Renderer))]
public class SpriteManager : MonoBehaviour {

    [Serializable]
    public class Sprite
    {
        public Texture SpriteSheet;
        public int columns;
        public int rows;
        
    }

    public Texture spritePackage;
    public int columnsInSpriteSheet = 14;
    public int rowsInSpriteSheet = 5;

    public Vector2[] transitionStates;

    private int currentState;
    private Vector2 size;
    private float nextFrame;


    private Material material;

	// Use this for initialization
	void Start () {
        material = renderer.material;

        material.SetTexture("_MainTex", spritePackage);
        size = new Vector3(1.0f / columnsInSpriteSheet, 1.0f / rowsInSpriteSheet);
        material.SetTextureScale("_MainTex", size); 

        // Set initial frame
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void SwitchFrame()
    {

    }
}
