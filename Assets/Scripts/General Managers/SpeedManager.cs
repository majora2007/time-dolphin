using UnityEngine;
using System.Collections;

public class SpeedManager : MonoBehaviour {

    public float speedUpdateInterval = 15.0f;
    public float speedStepSize = 1.0f;

    public static float Speed;
    private float nextUpdate;

	// Use this for initialization
	void Start () {

        //if GameStarted then
        nextUpdate = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (nextUpdate >= Time.time)
        {
            Speed += speedStepSize;
            nextUpdate = Time.time + speedUpdateInterval;
        }
	}
}
