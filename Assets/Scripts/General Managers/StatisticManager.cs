using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatisticManager : MonoBehaviour {

    //public List<string> events;

    private GameObject instance = null;

    public static int CollectedFish;
    public static int DamageTaken;
    public static int ObstaclesHit;
    public static int ManModeCount;
    public static int HeartsCollected;

    #region Score
    public static float DistanceTraveled;
    public static int Score;
    public static int MaxScore;
    #endregion
    


    void Awake()
    {
        
        if (instance == null)
        {
            instance = GameObject.Find("Statistic Manager");

            if (instance == null)
            {
                instance = new GameObject("Statistic Manager");
                instance.AddComponent<StatisticManager>();
                DontDestroyOnLoad(instance);
            }
            
        }
    }

	// Use this for initialization
	void Start () {
	    
        // Register to events
        Messenger.AddListener<int>(EventDefines.FISH_COLLECTED, FishUpdated);
        Messenger.AddListener<int>(EventDefines.OBSTACLE_HIT, ObstacleHit);
        Messenger.AddListener<bool>(EventDefines.MAN_MODE_ACTIVATED, ManModeActivated);
        Messenger.AddListener<int>(EventDefines.HEART_COLLECTED, HeartCollected);
        Messenger.AddListener<float>(EventDefines.DISTANCE_TRAVELED, DistanceTraveledUpdate);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FishUpdated(int fishes)
    {
        CollectedFish += fishes;
    }

    public void ObstacleHit(int damage)
    {
        DamageTaken += damage;
        ObstaclesHit++;
    }

    public void ManModeActivated(bool activated)
    {
        if (activated)
            ManModeCount++;
    }

    public void HeartCollected(int lifeRestored)
    {
        HeartsCollected++;
    }

    public void DistanceTraveledUpdate(float newDistance)
    {
        DistanceTraveled = newDistance;
    }
}
