using UnityEngine;
using System.Collections;

public class Life : MonoBehaviour {

    public int lifeRestored = 1;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Messenger.Broadcast<int>(EventDefines.LIFE_INCREASED, lifeRestored);
            Messenger.Broadcast<int>(EventDefines.HEART_COLLECTED, lifeRestored);
            Destroy(gameObject);
        }
    }
}
