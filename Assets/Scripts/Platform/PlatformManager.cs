using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlatformManager : MonoBehaviour
{

    public enum FunctionOption
    {
        Fish,
        Life,
        Obstacle
    }

    [Serializable]
    public class Feature
    {
        public Transform Prefab;
        public float SpawnProbability;
        public FunctionOption function;
    }


    #region PlatformMembers
    public Transform platformPrefab;
    public int numberOfObjects;
    public float recycleOffset;
    public Vector3 minSize, maxSize, minGap, maxGap;
    public float minY, maxY;
    public float zPosition;

    private Vector3 nextPlatformPosition;
    private Queue<Transform> platformQueue;
    #endregion

    // Feature Generation
    #region FeatureGeneration
    public int maxFeaturesPerPlatform = 2;
    public Feature[] featurePrefabs;
    private float[] featureProbs = new float[1];
    private delegate void FunctionDelegate(Transform currentPlatform, Transform prefab, float zPosition);
    private static FunctionDelegate[] functionDelegates = {
        GenerateFish,
        GenerateLife,
        GenerateObstacle
    };
    #endregion

    

	// Use this for initialization
	void Start () {
        platformQueue = new Queue<Transform>(numberOfObjects);

        AllocatePlatforms();

        nextPlatformPosition = transform.localPosition;

        for (int i = 0; i < numberOfObjects; i++)
        {
            GeneratePlatform();
        }

        featureProbs = new float[featurePrefabs.Length];
        float totalProb = 0.0f;
        for (int i = 0; i < featurePrefabs.Length; i++ )
        {
            totalProb += featurePrefabs[i].SpawnProbability;
            featureProbs[i] = featurePrefabs[i].SpawnProbability;
        }

        if (totalProb != 100.0f)
        {
            Debug.LogError("Feature Generation Probabilities must add up to 100.");
        }
	}

    private void AllocatePlatforms()
    {
        for (int i = 0; i < numberOfObjects; i++)
        {
            platformQueue.Enqueue((Transform)Instantiate(platformPrefab));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (platformQueue.Peek().localPosition.x + recycleOffset < Runner.DistanceTraveled)
        {
            GeneratePlatform();
        }
	}

    private Transform GeneratePlatform()
    {
        Vector3 scale = new Vector3(
            UnityEngine.Random.Range(minSize.x, maxSize.x),
            UnityEngine.Random.Range(minSize.y, maxSize.y),
            UnityEngine.Random.Range(minSize.z, maxSize.z));

        Vector3 position = nextPlatformPosition;
        position.x += scale.x * 0.5f;
        position.y += scale.y * 0.5f;
        position.z = zPosition;

        Transform platform = platformQueue.Dequeue();
        platform.localScale = scale;
        platform.localPosition = position;
        platformQueue.Enqueue(platform);

        // Generate Feature Set 
        GenerateFeature(platform);

        nextPlatformPosition += new Vector3(
            UnityEngine.Random.Range(minGap.x, maxGap.x) + scale.x,
            UnityEngine.Random.Range(minGap.y, maxGap.y),
            UnityEngine.Random.Range(minGap.z, maxGap.z));

        if (nextPlatformPosition.y < minY)
        {
            nextPlatformPosition.y = minY + maxGap.y;
        }
        else if (nextPlatformPosition.y > maxY)
        {
            nextPlatformPosition.y = maxY - maxGap.y;
        }

        return platform;
    }

    private void GenerateFeature(Transform currentPlatform)
    {
        if (currentPlatform == null)
        {
            Debug.Log("Feature cannot generate, platform is null.");
            return;
        }

        

        // Calculate how many features we are generating
        int numberOfFeaturesToGenerate = (int) UnityEngine.Random.Range(0.0f, maxFeaturesPerPlatform * 1.0f);
        if (numberOfFeaturesToGenerate == 0) return;

        for (int i = 0; i < numberOfFeaturesToGenerate; i++)
        {
            int j = Choose(featureProbs);
            FunctionDelegate calcFunction = functionDelegates[(int)featurePrefabs[j].function];

            calcFunction(currentPlatform, featurePrefabs[j].Prefab, zPosition);
        }
    }

    private int Choose(float[] probs)
    {
        float total = 0;

        for (int i = 0; i < probs.Length; i++)
        {
            total += probs[i];
        }

        float randomPoint = UnityEngine.Random.value * total;

        for (int i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else
                randomPoint -= probs[i];
        }

        return probs.Length - 1;
    }

    private static void GenerateFish(Transform platform, Transform prefab, float zPosition)
    {
        Vector3 position = new Vector3(0, 0, zPosition);
        Vector3 randomPoint = new Vector3();
        float fishYOffset = 2.5f;

        float m0 = platform.localPosition.x - (platform.localScale.x / 2);
        float m1 = platform.localPosition.x + (platform.localScale.x / 2);
        float m0T = m0 + (prefab.localScale.x / 2);
        float m1T = m1 - (prefab.localScale.x / 2);

        randomPoint.x = UnityEngine.Random.Range(m0T, m1T);
        randomPoint.y = UnityEngine.Random.Range(platform.localPosition.y + (platform.localScale.y / 2), platform.localPosition.y + (platform.localScale.y / 2) + fishYOffset);
        randomPoint.z = zPosition;

        position.x = randomPoint.x;
        position.y = randomPoint.y;
        position.z = randomPoint.z;

        Instantiate(prefab, position, prefab.localRotation);
        
    }

    private static void GenerateObstacle(Transform platform, Transform prefab, float zPosition)
    {
        Vector3 position = new Vector3(0, 0, zPosition);
        Vector3 randomPoint = new Vector3();

        float m0 = platform.localPosition.x - (platform.localScale.x / 2);
        float m1 = platform.localPosition.x + (platform.localScale.x / 2);
        float m0T = m0 + (prefab.localScale.x / 2);
        float m1T = m1 - (prefab.localScale.x / 2);

        randomPoint.x = UnityEngine.Random.Range(m0T, m1T);
        randomPoint.y = platform.localPosition.y + (platform.localScale.y / 2) + (prefab.localScale.y / 2);
        randomPoint.z = zPosition;

        position.x = randomPoint.x;
        position.y = randomPoint.y;
        position.z = randomPoint.z;

        Instantiate(prefab, position, prefab.localRotation);
    }

    private static void GenerateLife(Transform platform, Transform prefab, float zPosition)
    {

        Vector3 position = new Vector3(0, 0, zPosition);
        Vector3 randomPoint = new Vector3();
        float fishYOffset = 2.5f;

        float m0 = platform.localPosition.x - (platform.localScale.x / 2);
        float m1 = platform.localPosition.x + (platform.localScale.x / 2);
        float m0T = m0 + (prefab.localScale.x / 2);
        float m1T = m1 - (prefab.localScale.x / 2);

        randomPoint.x = UnityEngine.Random.Range(m0T, m1T);
        randomPoint.y = UnityEngine.Random.Range(platform.localPosition.y + (platform.localScale.y / 2), platform.localPosition.y + (platform.localScale.y / 2) + fishYOffset);
        randomPoint.z = zPosition;

        position.x = randomPoint.x;
        position.y = randomPoint.y;
        position.z = randomPoint.z;

        Instantiate(prefab, position, prefab.localRotation);
    }
}
