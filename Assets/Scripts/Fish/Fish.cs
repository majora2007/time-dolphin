using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class Fish : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Messenger.Broadcast<int>(EventDefines.FISH_COLLECTED, 1);
            Destroy(gameObject);
        }
    }
}
