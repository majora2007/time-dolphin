using UnityEngine;
using System.Collections;

public class Runner : MonoBehaviour {


    public static float DistanceTraveled;
    public static int FishCollected;
    public static int Lives = 3;
    private bool isManMode = false;



    public Texture spritePackage;
    public int columnsInSpriteSheet = 14;
    public int rowsInSpriteSheet = 5;
    public Vector3 jumpVelocity;

    
    private Vector2 WALK0_PIXEL;
    private Vector2 WALK1_PIXEL;
    private int walkState = 0;
    private Vector2 size;
    private float nextFrame;

    private bool touchingPlatform;
    private bool touchingSide;


    public static Vector2 WALK0 = new Vector2(9, 1);
    public static Vector2 WALK1 = new Vector2(10, 1);


    private float manModeTimer;



	// Use this for initialization
	void Start () {
        renderer.material.SetTexture("_MainTex", spritePackage);
        size = new Vector3(1.0f / columnsInSpriteSheet, 1.0f / rowsInSpriteSheet);

        WALK0_PIXEL = new Vector2(WALK0.x * size.x, 1.0f - WALK0.y * size.y);
        WALK1_PIXEL = new Vector2(WALK1.x * size.x, 1.0f - WALK1.y * size.y);

        renderer.material.SetTextureOffset("_MainTex", WALK0_PIXEL);
        renderer.material.SetTextureScale("_MainTex", size); 
        walkState = 0;
        nextFrame = Time.time;

        // Register with Event Manager
        Messenger.AddListener<int>(EventDefines.FISH_COLLECTED, FishUpdated);
        Messenger.AddListener<int>(EventDefines.OBSTACLE_HIT, Damaged);
        Messenger.AddListener<bool>(EventDefines.MAN_MODE_ACTIVATED, ManModeActivated);
        Messenger.AddListener<int>(EventDefines.HEART_COLLECTED, HeartCollected);
	}
	
	// Update is called once per frame
	void Update () {

        if (Lives <= 0)
        {
            // Send GameOver event
            return;
        }

        if (isManMode && Time.time >= manModeTimer)
        {
            // Swap texture back to original
            isManMode = false;
        }


        if (touchingSide)
        {
            //Debug.Log("Moving down");
            transform.Translate(0f, -10f * Time.deltaTime, 0f);
            return;
        }
        
        
        DistanceTraveled = transform.localPosition.x;
        Messenger.Broadcast<float>(EventDefines.DISTANCE_TRAVELED, DistanceTraveled);
        

        if (walkState == 0 && Time.time >= nextFrame)
        {
            renderer.material.SetTextureOffset("_MainTex", WALK1_PIXEL);
            walkState = 1;
            nextFrame = Time.time + 0.5f;
        }
        else if (walkState == 1 && Time.time >= nextFrame)
        {
            renderer.material.SetTextureOffset("_MainTex", WALK0_PIXEL);
            walkState = 0;
            nextFrame = Time.time + 0.5f;
        }

	}

    void FixedUpdate()
    {
        if (touchingPlatform && Input.GetAxis("Jump") > 0.0f)
        {
            rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);

            touchingPlatform = false;
        }

        if (touchingPlatform && !touchingSide)
        {
            rigidbody.AddForce(SpeedManager.Speed, 0f, 0f, ForceMode.Acceleration);
            //transform.Translate(0.0f, 0.0f, SpeedManager.Speed * Time.deltaTime);
        }
    }

    void OnCollisionEnter(Collision collisionInfo)
    {



        Transform platform = collisionInfo.gameObject.transform;

        if (collisionInfo.gameObject.tag == "Platform" && isTouchingSide(platform))
        {
            Debug.Log("Hit side platform!");
            touchingSide = true;
            rigidbody.velocity = Vector3.zero;
        }
        else
        {
            touchingPlatform = true;
        }
       
    }

    private bool isTouchingSide(Transform platform)
    {
        bool result = false;

        float m0x = platform.localPosition.x - (platform.localScale.x / 2);

        result = transform.localPosition.x + (transform.localScale.x / 2) <= m0x;
        
        return result;
        
    }

    void OnCollisionExit()
    {
        touchingPlatform = false;
        //touchingSide = false;
        //Debug.Log("No longer on platform!");
    }

    public void FishUpdated(int fishes)
    {
        FishCollected += fishes;
    }

    public void Damaged(int damageAmt)
    {
        if (!isManMode)
        {
            Lives -= damageAmt;
        }
    }

    public void ManModeActivated(bool activated)
    {
        isManMode = activated;
        manModeTimer = Time.time + 1.0f;
    }

    public void HeartCollected(int lifeAmt)
    {
        Lives += lifeAmt;
    }
}
