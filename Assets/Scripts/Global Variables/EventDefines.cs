using UnityEngine;
using System.Collections;

public static class EventDefines {

    public static string OBSTACLE_HIT = "ObstacleHit";

    //public static string HEALTH_DAMAGE = "HealthDamage"; // Replaced with OBSTACLE_HIT to be more precise.

    public static string FISH_COLLECTED = "FishCollected";

    //public static string LIFE_INCREASED = "LifeIncreased"; // Replaced with HEART_COLLECTED to be more precise.
    public static string HEART_COLLECTED = "HeartCollected";

    public static string MAN_MODE_ACTIVATED = "ManModeActivated";

    public static string MAN_MODE_DEACTIVATED = "ManModeDeactivated";

    public static string DISTANCE_TRAVELED = "DistanceTraveled";




}
