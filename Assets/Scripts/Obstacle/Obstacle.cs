using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class Obstacle : MonoBehaviour {

    public int Damage = 1;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Messenger.Broadcast<int>(EventDefines.OBSTACLE_HIT, Damage);
        }
        else if (other.gameObject.tag == "Projectile")
        {
            Destroy(gameObject);
        }
    }
}
