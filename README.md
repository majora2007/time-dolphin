Time Dolphin is an infinite runner action comedy game
similar to monster dash. You play as time dolphin, a
genetically awesome dolphin that has a jetpack and
turret gun strapped to his back, as he makes his way
through three distinct time periods killing demonic
cyborgs and collecting tasty fish.
the gameplay is simple. jump to avoid obstacles and
shoot to kill enemies to gain points which levels you
up.
each time period also has a boss which is unlocked
once you reach a certain level.
the point of the game is to survive as long as
possible, unlock gear, defeat all of the bosses, earn
highscores, and climb the leaderboards.
